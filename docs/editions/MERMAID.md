---
title: Mermaid schematics live editor
description : Mermaid schematics live editor
---


## References

- [Mermaid Home](https://mermaid-js.github.io/mermaid/#/)
- [Mermaid Live Editor](https://github.com/mermaid-js/mermaid-live-editor.git)


## How to build the Apps

```console
git clone https://github.com/mermaid-js/mermaid-live-editor.git
```

```console
cd mermaid-live-editor
docker build . -f Dockerfile -t mermaid-live-editor:latest
```

## How to run the Apps

```console
docker run --rm --name mermaid-live-editor -p 30080:80 mermaid-live-editor:latest
```

or with podman pod 

```console
podman pod create --name mermaid -p 1080:80
podman run -dt --pod mermaid mermaid-live-editor:latest
```
