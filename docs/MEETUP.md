---
title: Meetups
description : List of interesting meetup (passed or planned)
---

# jeu. 19 nov. 2020 à 18:30

- [Meetup virtuel AWS #7 - Automatisation, orchestration et gouvernance sur AWS](https://www.meetup.com/fr-FR/AWS-Lyon-Amazon-Web-Services-User-Group/events/274507446/)

- [awscon-onepager](https://github.com/zoph-io/awscon-onepager)

# Wed, Nov 25, 2020, 7:00 PM

- [S01E01: comment deployer dans Kubernetes avec les outils (nouveaux) Hashicorp](https://www.meetup.com/CNCF-Lyon-meetup/events/274034059/)
