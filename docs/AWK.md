# AWK tips and faq

## How to combine multiple line

Combine multiple line begining with different pattern into single line

```console
# awk '/^10\.|^159|^161|^166/{if (x)print x;x="";}{x=(!x)?$0:x","$0;}END{print x;}' fra-route.txt
``` 

Initial file format

```
 more fra-route.txt 
0.0.0.0/0          *[Static/5] 29w2d 20:12:24
                    >  to 159.122.100.121 via reth1.0
10.0.0.0/8         *[Static/5] 29w2d 20:12:24
                    >  to 10.135.3.193 via reth0.0
10.55.46.155/32    *[Static/5] 15:24:13
                    >  via st0.0
                    [Static/5] 7w0d 03:48:24
                    >  via st0.0
10.55.240.211/32   *[Static/5] 15:28:13
```


Target file format
```
0.0.0.0/0          *[Static/5] 29w2d 20:12:24,                    >  to 159.122.100.121 via reth1.0
10.0.0.0/8         *[Static/5] 29w2d 20:12:24,                    >  to 10.135.3.193 via reth0.0
10.55.46.155/32    *[Static/5] 15:24:13,                    >  via st0.0,                    [Static/5] 7w0d 03:48:24,                    >  via st0.0
10.55.240.211/32   *[Static/5] 15:28:13,                    >  via st0.0,                    [Static/5] 7w0d 03:48:24,                    >  via st0.0

```



## Reference

(AWK Reference guide)[https://www.theunixschool.com/2012/05/awk-join-or-merge-lines-on-finding.html]