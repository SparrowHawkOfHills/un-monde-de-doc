---
title: Marp Presentation 
description : Marp Presentation toolkit 
---
# Marp Markdown Presentation builder

## Objective

- Provide capabilities to create Presentation as a Code : PRAAC
- PDF / HTLM5 Rendering

## Requirements

- A dedicated apps or plugin for VSCODE to facilitate usage and rendering
- a CLI use to script build of presentation

## Thru VSCODE extension

## Thru CLI

We recommend to install Marp CLI into your Node project. You may control the CLI (and engine) version exactly.

```console
npm install --save-dev @marp-team/marp-cli
```

The installed marp command is available in npm-scripts or npx marp.

A more portable approach, use an official Docker image : **marpteam/marp-cli**

```console
docker pull marpteam/marp-cli
```

## Example 

```markdown
    ---
    theme: gaia
    _class: lead
    paginate: true
    backgroundColor: #fff
    backgroundImage: url('https://marp.app/assets/hero-background.jpg')
    ---

    ![bg left:40% 80%](https://marp.app/assets/marp.svg)

    # **Marp**

    Markdown Presentation Ecosystem

    https://marp.app/

    ---

    # How to write slides

    Split pages by horizontal ruler (`---`). It's very simple! :satisfied:

    ```markdown
    # Slide 1

    foobar

    ---

    # Slide 2

    foobar
    ```

```

- [.md](./examples/marp/example-marp.md)
- [Single html page](./examples/marp/example-marp.html)
- [.pdf](./examples/marp/example-marp.pdf)

## References

- [MARP WebSite](https://marp.app/)
- [MARP CLI](https://github.com/marp-team/marp-cli)
- [MARP CLI Binaries](https://github.com/marp-team/marp-cli/releases binaries)
