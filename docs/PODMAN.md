---
title: Podman
description : Podman usage
---
# Scope 

- Run an application and daemonize it with podman instead of docker
- GuestOS : CentOS 8


## Upgrade to the latest version of podman 

CentOS 8 provide an 1.6 version of podman, with some lacks.

The upgrade shift at least to the 2.1 version

```console
dnf -y module disable container-tools
dnf -y install 'dnf-command(copr)'
dnf -y copr enable rhcontainerbot/container-selinux
curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/CentOS_8/devel:kubic:libcontainers:stable.repo
dnf -y install podman
```

### Sample Apps 

As a standard user you can use podman pod to start and daemonize an apps.

Exposed TCP/IP ports should be other 1024

```console
podman pod create --name portal -p 1443:443
podman run -dt --pod portal -v heimdall-data:/config linuxserver/heimdall:latest
```
