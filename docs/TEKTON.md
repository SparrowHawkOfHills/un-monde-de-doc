---
title: Tekton Pipeline  
description : Tekton Pipeline  
---
## Reference

[Start Here](https://tekton.dev/)

[Git Repo](https://github.com/tektoncd)

[Tetkon tasks catalog (Tasks Hub)](https://hub-preview.tekton.dev/)

- argocd
- ansible tower cli
- gitlab add label
- ibmcloud
- kubernetes actions
- prometheus gate
- sonarqube scanner
- terraform cli

## Internal Components

[Task versus ClusterTask](https://github.com/tektoncd/pipeline/blob/master/docs/tasks.md#task-vs-clustertask)

## Installation of Control Plane

```console
kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
```

## Openshift operator on minikube

[Tektoncd-pipeline-operator](https://github.com/openshift/tektoncd-pipeline-operator)

## Dashboard

https://github.com/tektoncd/dashboard

